# cocoafishmoduleandroid Module

## Description

TODO: Enter your module description here

## Accessing the cocoafishmoduleandroid Module

To access this module from JavaScript, you would do the following:

	var cocoafishmoduleandroid = require("com.ci.cocoafish");

The cocoafishmoduleandroid variable is a reference to the Module object.	

## Reference

TODO: If your module has an API, you should document
the reference here.

### ___PROJECTNAMEASIDENTIFIER__.function

TODO: This is an example of a module function.

### ___PROJECTNAMEASIDENTIFIER__.property

TODO: This is an example of a module property.

## Usage

TODO: Enter your usage example here

## Author

TODO: Enter your author name, email and other contact
details you want to share here. 

## License

TODO: Enter your license/legal information here.
